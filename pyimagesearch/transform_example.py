from transform import four_point_transform
import numpy as np
import argparse
import cv2

"""
image : (87,113),(287,33),(358,283),(560,178)
image2 : (244,3),(460,49),(4,153),(266,272)
example01 : (73, 239), (356, 117), (475, 265), (187, 443)
example02 : (101, 185), (393, 151), (479, 323), (187, 441)
example03 : (63, 242), (291, 110), (361, 252), (78, 386)
"""
#construct the argument parser
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", help = "path to the image file")
ap.add_argument("-c", "--coords", help = "comma seperated list of source points")
args = vars(ap.parse_args())

image = cv2.imread(args["image"])
pts = np.array(eval(args["coords"]), dtype = "float32")

warped = four_point_transform(image, pts)

#show the original
cv2.imshow("Original", image)
cv2.imshow("Warped", warped)
cv2.waitKey(0)
cv2.destroyAllWindows()