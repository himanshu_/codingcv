#import the packages
import numpy as np
import cv2

def order_points(pts):
    #Initialize the coordinates top-left, top-right, bottom-right, botton-left.
    rect = np.zeros((4,2), dtype="float32")

    #The top-left point will have smallest sum, bottom-right will have largest sum.
    s = pts.sum(axis=1)
    rect[0] = pts[np.argmin(s)]
    rect[2] = pts[np.argmax(s)]

    #The top-right point will have the smallest difference, bottom-left will have the largest difference.
    diff = np.diff(pts, axis=1)
    rect[1] = pts[np.argmin(diff)]
    rect[3] = pts[np.argmax(diff)]

    return rect

def four_point_transform(image, pts):
    #obtain the constistent order of the points
    rect = order_points(pts)
    (tl, tr, br, bl) = rect

    #obtain the maximum width either between tl and tr or br and bl
    widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
    widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
    maxWidth = max(int(widthA), int(widthB))

    #obtain the maximum height either between tl and bl or tr and br
    heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
    heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
    maxHeight = max(int(heightA),int(heightB))

    #construct the set of destination points
    dst = np.array([
        [0,0],
        [maxWidth-1,0],
        [maxWidth-1,maxHeight-1],
        [0, maxHeight-1]
    ], dtype="float32")

    #compute the perspective transform matrix
    M = cv2.getPerspectiveTransform(rect, dst)
    wrapped = cv2.warpPerspective(image, M, (maxWidth, maxHeight))

    return wrapped 